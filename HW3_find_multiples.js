
/* 1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
   
   Циклы позволяют производить повторяющиеся однотипные расчеты или действия, до тех пор, пока не будут достигнуты нужные условия, 
   или пока заданные условия не будут нарушены.
*/

/*  2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.

   Для перебора элементов массива удобно пользоваться циклом for - пробегаемся по всем элементам до тех пор, пока итератор не достигнет длинны массива.
   Если от пользователя необходимо получить корректные, то можно использовать цикл while и повторять запрос пока пользователь не введет данные, которые соответствуют заданным условиям.    
*/

/*  3. Що таке явне та неявне приведення (перетворення) типів даних у JS?
   Явное преобразование типов это когда мы с помощью специальных методов Number(), toString(), !!, унарный плюс и т.д. явно указываем, что хотим поменять тип каких то данных, 
   а не явное - это когда мы не указываем этого, а js сам приводит данные к одинаковым типам, например при использовании операторов сравнения. 
   
*/

function setNumber(text) {
   let number = +prompt(`Введіть ${text || "число"}`);
   while (!Number.isInteger(number) || isNaN(number)) {
      number = +prompt(`Введіть ${text || "корректне ціле число"}`);
   }
   return number;
}

// Расчет чисел кратных "5"
function multiplesOfFive(num) {
   let arr = [];
   for (let i = 1; i <= num; i++) {
      if (i % 5 === 0) {
         arr.push(i);
      }
   }
   !arr.length ? console.log("Sorry, no numbers") : console.log(`Всі числа, що діляться на "5" від 1 до ${num}: ${arr}`);
   return arr;
}

multiplesOfFive(setNumber());


// Обычный способ расчета простых чисел, от 0 до 1млн расчет занял 161сек
function primeNumbers(num1, num2) {
   let start = Date.now();
   const min = Math.min(num1, num2);
   const max = Math.max(num1, num2);
   let primes = [];
   nextNumber:
   for (let i = min; i <= max; i++) {
      for (let j = 2; j < i; j++) {
         if (i % j === 0) {
            continue nextNumber;
         }
      }
      if (i > 1) primes.push(i);
   }
   !primes.length ? console.log("Sorry, no numbers") : console.log(`Всі прості числа діапазону ${min}...${max}: ${primes}`);
   let end = Date.now();
   let estimatedTime = (end - start) / 1000;
   console.log(`${estimatedTime} seconds`);
   console.log(`Количество простых чисел ${primes.length}`);
   return primes;
}

primeNumbers(setNumber("перше число"), setNumber("друге число"));



// Все не простые числа можно разложить на простые множители, 
// поэтому если число не делится на все предыдущие простые числа, то оно простое + 
// массив простых чисел на которые мы делим также можно делить на 2, 
// потому что проверяемое число не поделится нацело на другое, которое составляет больше его половины.
//  
// Оптимизированный способ расчета простых чисел, от 0 до 1млн расчет занял 11сек
function primeNumberOptimize(num1, num2) {
   let start = Date.now();
   const min = Math.min(num1, num2);
   const max = Math.max(num1, num2);
   let primes = [];
   let result = [];
   nextNumber:
   for (let i = 2; i <= max; i++) {
      for (let j = 0; j <= Math.ceil(primes.length / 2); j++) {
         if (i % primes[j] === 0) {
            continue nextNumber;
         }
      }
      if (i > 0) primes.push(i);
      if (i >= min) {
         result.push(i);
      }
   }
   !result.length ? console.log("Sorry, no numbers") : console.log(`Всі прості числа діапазону ${min}...${max}: ${result}`);
   let end = Date.now();
   let estimatedTime = (end - start) / 1000;
   console.log(`${estimatedTime} seconds`);
   console.log(`Количество простых чисел ${result.length}`);
   return result;
}

primeNumberOptimize(setNumber("перше число"), setNumber("друге число"));